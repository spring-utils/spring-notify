package springnotify;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class TestToolboxIntegr
{
    public static File getResourceFile(String fileName) throws IOException
    {
        return new File(getResourceDir(), fileName);
    }

    public static File getResourceDir() throws IOException
    {
        return Paths.get("src", "integration-test", "resources").toFile();
    }
}
