package springnotify.mattermost;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import springnotify.TestToolboxIntegr;
import springnotify.util.NotifyFileUtils;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MattermostServiceTest {
	private final static String EXCEPTION_FILE_NAME = "mattermost-content.md";
	private final static String STACKTRACE_FILE = "error-stacktrace.txt";

	@Autowired
	private MattermostService mattermostService;

	@Autowired
	private NotifyFileUtils fileUtils;

	@Test
	public void try_exception_message() throws Exception {
		File resourceFile = TestToolboxIntegr.getResourceFile(STACKTRACE_FILE);
		String stackTrace = fileUtils.getFileContent(resourceFile);

		String date = "2017-10-25T11:22:32Z";
		String exceptionContent = mattermostService.createExceptionContent(stackTrace, date);

		resourceFile = TestToolboxIntegr.getResourceFile(EXCEPTION_FILE_NAME);
		String expectedResult = fileUtils.getFileContent(resourceFile);

		assertThat(exceptionContent).isEqualTo(expectedResult);
	}
}
