package springnotify.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

public class DateTimeServiceTest
{
    @Test
    public void test()
    {
        ZonedDateTime createdDateTime = DateTimeService.now().minusDays(1);
        String createDateTimeString = createdDateTime.format(DateTimeFormatter.ISO_INSTANT);
        long millisToNow = DateTimeService.calcMillisToNow(createDateTimeString);

        assertThat(millisToNow / 1000).isEqualTo(60 * 60 * 24);
    }
}
