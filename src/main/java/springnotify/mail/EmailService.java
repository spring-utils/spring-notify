package springnotify.mail;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import lombok.extern.flogger.Flogger;
import springnotify.config.MailConfiguration;

@Service
@Flogger
public class EmailService {
	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private MailConfiguration configuration;

	public void sendEmail(List<String> emailTo, String subject, List<String> messages, File attachmentFile,
			boolean html) {
		String[] mailToArray = emailTo.toArray(new String[] {});

		String message = messages.stream().collect(Collectors.joining("\n\n"));

		MimeMessagePreparator messagePreparator = mimeMessage -> {

			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
			messageHelper.setFrom(configuration.getFromMail());
			messageHelper.setTo(mailToArray);
			messageHelper.setSubject(subject);
			messageHelper.setText(message, html);

			if (attachmentFile != null) {
				FileSystemResource resourceFile = new FileSystemResource(attachmentFile);
				messageHelper.addAttachment(attachmentFile.getName(), resourceFile);
			}
		};

		send(emailTo, messagePreparator, subject);
	}

	public void sendEmail(String message, List<String> emailTo, String subject) {
		String[] mailToArray = emailTo.toArray(new String[] {});
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(configuration.getFromMail());
			messageHelper.setTo(mailToArray);
			messageHelper.setSubject(subject);
			messageHelper.setText(message, false);
		};
		send(emailTo, messagePreparator, subject);
	}

	private void send(List<String> emailTo, MimeMessagePreparator messagePreparator, String subject) {
		try {
			log.atInfo().log("Sending email to {}, " + subject, emailTo);
			emailSender.send(messagePreparator);
		} catch (MailException e) {
			log.atSevere().withCause(e);
		}
	}
}
