package springnotify.mattermost;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.extern.flogger.Flogger;
import springnotify.config.MattermostConfiguration;

@Flogger
@Component
public class MattermostClient {
	@Autowired
	private MattermostConfiguration configuration;

	public void sendReport(String message) {
		String webhook = configuration.getWebhookReport();
		sendMessage(message, webhook);
	}

	public void sendException(String message) {
		String webhook = configuration.getWebhookException();
		sendMessage(message, webhook);
	}

	private void sendMessage(String message, String webhook) {

		checkMessageLength(message);

		RestTemplate restTemplate = new RestTemplate();
		try {
			URI uri = new URI(webhook);
			restTemplate.postForEntity(uri, message, Void.class);
		} catch (Exception e) {
			log.atSevere().withCause(e);
		}
	}

	private void checkMessageLength(String message) {
		int charLimit = configuration.getCharLimit();
		if (message.length() > charLimit) {
			log.atWarning().log("Message has length {}, but max char size is {}.", message.length(), charLimit);
		}
	}
}
