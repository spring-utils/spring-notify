package springnotify.mattermost;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.flogger.Flogger;

@Flogger
@Service
public class TemplateParseService {
	@Autowired
	private Configuration freemarkerConfig;

	public String process(String fileName, Map<String, Object> model) {
		try {
			Template template = freemarkerConfig.getTemplate(fileName);
			StringWriterPerLine writer = new StringWriterPerLine();
			template.process(model, writer);
			return writer.getStringLinuxLineEnding();
		} catch (IOException | TemplateException e) {
			log.atSevere().withCause(e);
		}
		return "";
	}

	private static class StringWriterPerLine extends StringWriter {
		String getStringLinuxLineEnding() {
			String string = this.toString();
			return string.replaceAll("\r\n", "\n");
		}
	}
}
