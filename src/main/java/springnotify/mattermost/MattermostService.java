package springnotify.mattermost;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springnotify.config.MattermostConfiguration;
import springnotify.util.DateTimeService;

@Service
public class MattermostService {
	private static final String TEMPLATE_EXCEPTION = "reportingExceptionMattermost.ftl";

	@Autowired
	private TemplateParseService templateParseService;

	@Autowired
	private MattermostConfiguration mattermostConfiguration;

	@Autowired
	private MattermostClient client;

	public void sendException(String stackTrace) {
		String date = DateTimeService.nowAsString();
		String message = createExceptionContent(stackTrace, date);
		client.sendException(message);
	}

	String createExceptionContent(String stackTrace, String date) {
		int charLimit = mattermostConfiguration.getCharLimit();
		if (stackTrace.length() > charLimit - 200)// 200 chars for header and
													// timestamp
		{
			stackTrace = stackTrace.substring(0, charLimit - 200);
		}

		String title = "test-execution-service";

		Map<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("title", title);
		parameters.put("date", date);
		parameters.put("stacktrace", stackTrace);

		String message = templateParseService.process(TEMPLATE_EXCEPTION, parameters);
		return message;
	}
}
