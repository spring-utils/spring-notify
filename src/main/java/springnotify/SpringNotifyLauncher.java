package springnotify;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SpringNotifyLauncher {
	public static void main(String[] args) {
		new SpringApplicationBuilder(SpringNotifyLauncher.class).run(args);
	}
}
