package springnotify.util;

import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.springframework.util.StringUtils;

public class DateTimeService {
	/**
	 * UTC date time
	 */
	public static ZonedDateTime now() {
		return ZonedDateTime.now(ZoneOffset.UTC);
	}

	public static String nowAsString() {
		return createString(now());
	}

	public static long nowInMillis() {
		return Instant.now().toEpochMilli();
	}

	public static ZonedDateTime convertFileTime(FileTime fileTime) {
		return fileTime.toInstant().atZone(ZoneOffset.UTC);
	}

	public static long calcMillisToNow(String createdDateTimeUTC) {
		ZonedDateTime createdDateTime = Instant.parse(createdDateTimeUTC).atZone(ZoneOffset.UTC);
		return calcMillisToNow(createdDateTime);
	}

	public static long calcMillisToNow(ZonedDateTime dateTime) {
		if (dateTime == null) {
			return 0L;
		}
		return ChronoUnit.MILLIS.between(dateTime, now());
	}

	public static long calcSecToNow(ZonedDateTime dateTime) {
		if (dateTime == null) {
			return 0L;
		}
		return ChronoUnit.SECONDS.between(dateTime, now());
	}

	public static String generateDurationString(ZonedDateTime fromDate, ZonedDateTime tillDate) {
		if (fromDate != null && tillDate != null) {
			long seconds = ChronoUnit.SECONDS.between(fromDate, tillDate);

			return generateDurationString(seconds);
		}
		return "";
	}

	public static String generateDurationString(long seconds) {
		return String.format("%d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60, (seconds % 60));
	}

	public static ZonedDateTime parseString(String string) {
		if (StringUtils.hasLength(string)) {
			return ZonedDateTime.parse(string, DateTimeFormatter.ofPattern(JsonSerializer.JSON_ZONED_DATE_TIME));
		}
		return null;
	}

	public static String createString(ZonedDateTime zonedDateTime) {
		if (zonedDateTime != null) {
			return zonedDateTime.format(DateTimeFormatter.ofPattern(JsonSerializer.JSON_ZONED_DATE_TIME));
		}
		return "";
	}
}
