package springnotify.util;

import static java.util.stream.Collectors.joining;

import java.io.File;
import java.nio.file.Files;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import lombok.extern.flogger.Flogger;

@Flogger
@Component
public class NotifyFileUtils {

	public String getFileContent(File directory, String fileName) {
		File file = new File(directory, fileName);
		return getFileContent(file);
	}

	public String getFileContent(File file) {
		if (file == null || !file.exists()) {
			return "";
		}

		try (Stream<String> lines = Files.lines(file.toPath())) {
			return lines.collect(joining("\n"));
		} catch (Exception e) {
			log.atSevere().withCause(e);
		}
		return "";
	}
}
