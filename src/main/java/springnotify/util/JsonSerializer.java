package springnotify.util;

import java.io.IOException;
import java.time.ZonedDateTime;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import lombok.extern.flogger.Flogger;

@Flogger
public class JsonSerializer {
	/**
	 * ISO_INSTANT
	 */
	public static final String JSON_ZONED_DATE_TIME = "yyyy-MM-dd'T'HH:mm:ss.SSSVV";

	private static final DateTimeParser DATE_TIME_PARSER = new DateTimeParser();

	public static <T> T fromJson(String data, Class<T> clazz) {
		if (StringUtils.isBlank(data)) {
			return null;
		}

		try {
			Gson gson = createGson(false);
			return gson.fromJson(data, clazz);
		} catch (JsonSyntaxException e) {
			log.atSevere().withCause(e).log("Unable to parse string. " + data);
			return null;
		}
	}

	public static String toJson(Object data) {
		return toJson(data, true);
	}

	public static String toJson(Object data, boolean isPretty) {
		if (data == null) {
			return "";
		}

		try {
			Gson gson = createGson(isPretty);
			return gson.toJson(data);
		} catch (JsonSyntaxException e) {
			log.atSevere().withCause(e).log("Unable to create string.");
			return null;
		}
	}

	private static Gson createGson(boolean isPretty) {
		GsonBuilder builder = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, DATE_TIME_PARSER);
		if (isPretty) {
			builder.setPrettyPrinting();
		}
		return builder.create();
	}

	private static class DateTimeParser extends TypeAdapter<ZonedDateTime> {
		@Override
		public void write(JsonWriter out, ZonedDateTime value) throws IOException {
			String string = DateTimeService.createString(value);
			out.value(string);
		}

		@Override
		public ZonedDateTime read(JsonReader in) throws IOException {
			if (in.peek() == JsonToken.NULL) {
				in.nextNull();
				return null;
			}
			String string = in.nextString();
			return DateTimeService.parseString(string);
		}
	}
}
