package springnotify.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Getter
@Configuration
public class MattermostConfiguration {
	@Value("${mattermost.webhooks.report}")
	private String webhookReport;

	@Value("${mattermost.webhooks.exception}")
	private String webhookException;

	@Value("${mattermost.char-limit}")
	private int charLimit;
}
